var ranking = angular.module('ranking',
	[]
);

ranking.controller('RankingCtrl', function ($scope, $http) {
	$http.get('server/ranking/').success(function (data) {
		$scope.rankings = data;
	});
	
	$http.get('server/prova/').success(function (data) {
		$scope.provas = data;
	});

	$http.get('server/agenda/').success(function (data) {
		$scope.agenda = data;
	});
});

ranking.filter('colapsecontrol', function() {
	return function(input) {
		return input == 'S' ? '' : 'collapsed';
	};
});

ranking.filter('colapsepanel', function() {
	return function(input) {
		return input == 'S' ? 'in' : '';
	};
});
