<?php
require '../vendor/autoload.php';
require '../dbConn.php';

$app = new \Slim\Slim(array(
    'mode' => $_CONFIG["Slim"]["mode"],
	'debug' => $_CONFIG["Slim"]["debug"]
));
$app->response()->header('Content-Type', 'application/json;charset=utf-8');

$app->get('/', function () {
	$query = <<<QUERY
	SELECT 
		agd.id,
		agd.semana,
		agd.idProva,
		prv.nome as nomeProva,
		prv.link as linkProva
	FROM agenda agd
	INNER JOIN provas prv ON (agd.idProva = prv.id)
QUERY;
	
	$conn = DbConn::getConnection();
	$stmt = $conn->query($query);
	$agendas = $stmt->fetchAll(PDO::FETCH_OBJ);
	echo json_encode($agendas);
});


$app->get('/:id', function ($id) {
	$query = <<<QUERY
	SELECT 
		agd.id,
		agd.semana,
		agd.idProva,
		prv.nome as nomeProva,
		prv.link as linkProva
	FROM agenda agd
	INNER JOIN provas prv ON (agd.idProva = prv.id)
	WHERE agd.id = $id
QUERY;
	
	$conn = DbConn::getConnection();
	$stmt = $conn->query($query);
	$prova = $stmt->fetch(PDO::FETCH_OBJ);
	echo json_encode($prova);
});


$app->run();

?>