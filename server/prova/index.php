<?php
require '../vendor/autoload.php';
require '../dbConn.php';

$app = new \Slim\Slim(array(
    'mode' => $_CONFIG["Slim"]["mode"],
	'debug' => $_CONFIG["Slim"]["debug"]
));
$app->response()->header('Content-Type', 'application/json;charset=utf-8');

$app->get('/', function () {
	$query = <<<QUERY
	SELECT 
		prv.id,
		prv.nome,
		prv.banca,
		prv.link
	FROM provas prv
QUERY;
	
	$conn = DbConn::getConnection();
	$stmt = $conn->query($query);
	$provas = $stmt->fetchAll(PDO::FETCH_OBJ);
	echo json_encode($provas);
});


$app->get('/:id', function ($id) {
	$query = <<<QUERY
	SELECT 
		prv.id,
		prv.nome,
		prv.banca,
		prv.link
 	FROM provas prv
	WHERE prv.id = $id
QUERY;
	
	$conn = DbConn::getConnection();
	$stmt = $conn->query($query);
	$prova = $stmt->fetch(PDO::FETCH_OBJ);
	echo json_encode($prova);
});


$app->run();

?>