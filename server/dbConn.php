<?php
require 'config.php';
class DbConn
{
	private static $DbConnection = NULL;
	public static function getConnection() {
		if(DbConn::$DbConnection == NULL) {
			global $_CONFIG;
			DbConn::$DbConnection = new PDO(
				'mysql:host=' . $_CONFIG['DB']['host'] . ';port=' . $_CONFIG['DB']['port'] . ';dbname='.$_CONFIG['DB']['database'],
				$_CONFIG['DB']['user'],
				$_CONFIG['DB']['password'],
				array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
			);
		}
		return DbConn::$DbConnection;	
	}
}
?>