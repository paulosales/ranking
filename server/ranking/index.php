<?php
require '../vendor/autoload.php';
require '../dbConn.php';

$app = new \Slim\Slim(array(
    'mode' => $_CONFIG["Slim"]["mode"],
	'debug' => $_CONFIG["Slim"]["debug"]
));
$app->response()->header('Content-Type', 'application/json;charset=utf-8');

$app->get('/', function () {
	$query = <<<QUERY
	SELECT 
		rks.id,
		rks.nome,
		rks.ativo
	FROM rankings rks
QUERY;
	
	$conn = DbConn::getConnection();
	$stmt = $conn->query($query);
	$rankings = $stmt->fetchAll(PDO::FETCH_OBJ);
	echo json_encode($rankings);
});


$app->run();

?>